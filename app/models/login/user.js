"use strict";
var User = (function () {
    function User(userInfo) {
        this.name = userInfo.name;
        this.password = userInfo.password;
    }
    return User;
}());
exports.User = User;

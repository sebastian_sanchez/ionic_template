import {IonicApp, Page, NavController} from 'ionic-angular';
import { Component } from 'angular2/core'; 
import { FORM_DIRECTIVES, FormBuilder,  ControlGroup, Validators, AbstractControl } from 'angular2/common';
import {HelloIonicPage} from '../hello-ionic/hello-ionic';
import {User} from '../../models/login/user';


@Page({
  templateUrl: 'build/pages/login/login.html',
   directives: [FORM_DIRECTIVES]
})
export class LoginPage {


    authForm: ControlGroup;
    username: AbstractControl;
    password: AbstractControl;
    nav: any;

  static get parameters() {
    return [[NavController],[FormBuilder]];
  }

   constructor(nav,fb) {

     this.nav = nav;
        this.authForm = fb.group({  
            'username': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
        });
 
        this.username = this.authForm.controls['username'];     
        this.password = this.authForm.controls['password'];     

        console.log('Submitted value: ' + this.username  + '.....'+ this.password);
    }


 onSubmit(value: string): void { 
        if(this.authForm.valid) {

        window.localStorage.setItem('username', this.username.value);
        window.localStorage.setItem('password', 'aaaaa');

        this.nav.push(HelloIonicPage);  

            console.log('Submitted value: ' + this.username.value);
        }
    }     

}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var common_1 = require('angular2/common');
var hello_ionic_1 = require('../hello-ionic/hello-ionic');
var LoginPage = (function () {
    function LoginPage(nav, fb) {
        this.nav = nav;
        this.authForm = fb.group({
            'username': ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(8)])],
            'password': ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(8)])]
        });
        this.username = this.authForm.controls['username'];
        this.password = this.authForm.controls['password'];
        console.log('Submitted value: ' + this.username + '.....' + this.password);
    }
    Object.defineProperty(LoginPage, "parameters", {
        get: function () {
            return [[ionic_angular_1.NavController], [common_1.FormBuilder]];
        },
        enumerable: true,
        configurable: true
    });
    LoginPage.prototype.onSubmit = function (value) {
        if (this.authForm.valid) {
            window.localStorage.setItem('username', this.username.value);
            window.localStorage.setItem('password', 'aaaaa');
            this.nav.push(hello_ionic_1.HelloIonicPage);
            console.log('Submitted value: ' + this.username.value);
        }
    };
    LoginPage = __decorate([
        ionic_angular_1.Page({
            templateUrl: 'build/pages/login/login.html',
            directives: [common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [Object, Object])
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;

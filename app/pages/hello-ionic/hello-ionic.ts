import { Page, NavController, NavParams } from 'ionic-angular';
import { Component } from 'angular2/core';  
import { LoginPage } from '../login/login';


@Page({
	templateUrl: 'build/pages/hello-ionic/hello-ionic.html'
})

export class HelloIonicPage {

	nav: any 
	username: String 
	constructor(nav: NavController, navParams: NavParams) {
	  this.nav = nav;
	  this.username = window.localStorage.getItem('username');   
	}

	logout(): void { 
  	window.localStorage.removeItem('username');
  	window.localStorage.removeItem('password');

		this.nav.setRoot(LoginPage);
	  this.nav.popToRoot();   
	} 
}